/**
 * Padding outputs 2 characters allways
 * @param {string} hex one or two characters
 * @returns {string} hex with two characters
 */



module.exports = {
    /**
     * Converts RGB to Hex string
     * @param {number} red 0-255
     * @param {number} green 0-255
     * @param {number} blue 0-255
     * @returns {string} hex value
     */
    HexToRGB: (hex) => {
        console.log({hex})
        const red = parseInt(hex.substring(0, 2), 16); 
        const green = parseInt(hex.substring(2, 4), 16);
        const blue = parseInt(hex.substring(4, 6), 16);
        return [red, green, blue];
    }
}
